package com.hcl.generics;

public class MyGeneric<T> {
	private T object;

	public MyGeneric(T object) {
		super();
		this.object = object;
	}

	public T getObject() {
		//System.out.println(object.toString());
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}
	

}
