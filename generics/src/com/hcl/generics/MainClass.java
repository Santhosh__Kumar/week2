package com.hcl.generics;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
public class MainClass {

	public static void main(String[] args) {
		/*
		 * MyGeneric<Customer>myobj=new MyGeneric<Customer>(new
		 * Customer("sk",123,"Platinum")); System.out.println(myobj.getObject());
		 */
		ArrayList<MyGeneric>list=new ArrayList<MyGeneric>();
		list.add(new MyGeneric<Customer>(new Customer("sk",123,"Platinum")));
		list.add(new MyGeneric<Customer>(new Customer("sam",234,"Gold")));
		list.add(new MyGeneric<Customer>(new Customer("Harish",567,"Black")));
		for(MyGeneric g:list) {
			System.out.println(g.getObject());
			System.out.println();
		}
		System.out.println();
		Map<String,MyGeneric>map=new TreeMap<String,MyGeneric>();
		map.put("E1",new MyGeneric(new Employee(123,"Santhosh")));
		map.put("E3",new MyGeneric(new Employee(234,"Arun")));
		map.put("E2",new MyGeneric(new Employee(456,"Vicky")) );
		Set <String> set=map.keySet();
		for(String s:set) {
			System.out.println(map.get(s).getObject()+" "+s);
			System.out.println();
		}
		
		
	}
	

}
