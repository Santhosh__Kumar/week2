package com.hcl.generics;

public class Customer {
	private String name;
	private int id;
	private String subscription;
	public Customer(String name, int id, String subscription) {
		super();
		this.name = name;
		this.id = id;
		this.subscription = subscription;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public String toString() {
		System.out.println(name);
		System.out.println(id);
		return subscription;
	}

}
