package com.hacl.userexception;

import java.util.ArrayList;

public class Customer {
	private String userName;
	private String userId;
	private long accNo;
	private double availableBalance;
	public Customer() {
		super();
	}
	public Customer(String userName, String userId, long accNo, double availableBalance) {
		super();
		this.userName = userName;
		this.userId = userId;
		this.accNo = accNo;
		this.availableBalance = availableBalance;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getAccNo() {
		return accNo;
	}
	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}
	public double getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}
	public  Customer searchUser(ArrayList<Customer>customerList,String name) {
		for (Customer c:customerList) {
			if(c.getUserName().equalsIgnoreCase(name)) {
				return c;
			}
			
		}
		return null;
	}
	public Customer passwordVerification(ArrayList<Customer>list,Long pass) {
		for(Customer c:list) {
			if(c.getAccNo()==pass) {
				return c;
			}
		}
		return null;
	}
	public void addAmount(Customer customer,Double newBalance) {
		customer.availableBalance+=newBalance;
		System.out.println("Your current balance: "+customer.getAvailableBalance());
	}
	

}
