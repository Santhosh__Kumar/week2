package com.hcl.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedExceptionClass {

	public static void main(String[] args) {
		FileReader reader=null;
		try {
			reader =new FileReader("src/hello.txt");
		}
		catch(FileNotFoundException fe){
			System.out.println("File not found");
			
		}
		System.out.println(reader);

	}

}
