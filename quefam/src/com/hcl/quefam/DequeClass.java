package com.hcl.quefam;
import java.util.Deque;

import java.util.ArrayDeque;
public class DequeClass {

	public static void main(String[] args) {
		Deque<String> dq=new ArrayDeque<String>();
		dq.offerFirst("Abdul");
		dq.offerLast("Siddarth");
		dq.offer("Daro");
		dq.offer("Sk");
		System.out.println(dq);
		System.out.println();
		System.out.println(dq.pollFirst());
		System.out.println(dq);
		System.out.println();
		System.out.println(dq.pollLast());
		System.out.println(dq);
		

	}

}
